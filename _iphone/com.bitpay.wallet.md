---
wsId: bitpaywallet
title: "BitPay – Buy Crypto"
altTitle: 
authors:
- leo
appId: com.bitpay.wallet
idd: 1149581638
released: 2016-10-21
updated: 2021-03-06
version: "12.3.2"
score: 4.02048
reviews: 1074
size: 86147072
developerWebsite: https://bitpay.com
repository: 
issue: 
icon: com.bitpay.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

