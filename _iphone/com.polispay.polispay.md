---
wsId: PolisPay
title: "PolisPay - Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.polispay.polispay
idd: 1351572060
released: 2019-02-17
updated: 2021-01-02
version: "8.8.0"
score: 3.83333
reviews: 6
size: 34923520
developerWebsite: 
repository: 
issue: 
icon: com.polispay.polispay.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

