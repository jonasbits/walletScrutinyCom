---
wsId: indodax
title: "Indodax Trading Platform"
altTitle: 
authors:
- leo
appId: id.co.bitcoin.Bitcoin-Trading-Platform
idd: 1349104693
released: 2018-03-26
updated: 2021-02-18
version: "2.2.4"
score: 
reviews: 
size: 73229312
developerWebsite: https://indodax.com
repository: 
issue: 
icon: id.co.bitcoin.Bitcoin-Trading-Platform.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

