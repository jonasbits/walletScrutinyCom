---
wsId: ViaWallet
title: "ViaWallet - Multi-chain Wallet"
altTitle: 
authors:
- leo
appId: com.viabtc.ViaWallet
idd: 1462031389
released: 2019-05-18
updated: 2021-01-25
version: "2.2.5"
score: 4.07692
reviews: 13
size: 77724672
developerWebsite: https://viawallet.com
repository: 
issue: 
icon: com.viabtc.ViaWallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

