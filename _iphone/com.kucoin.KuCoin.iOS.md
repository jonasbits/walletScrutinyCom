---
wsId: kucoin
title: "KuCoin"
altTitle: 
authors:
- leo
appId: com.kucoin.KuCoin.iOS
idd: 1378956601
released: 2018-05-11
updated: 2021-03-06
version: "3.28.1"
score: 3.67282
reviews: 379
size: 245865472
developerWebsite: 
repository: 
issue: 
icon: com.kucoin.KuCoin.iOS.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: KuCoinCom
providerLinkedIn: company/kucoin
providerFacebook: KuCoinOfficial
providerReddit: kucoin

redirect_from:

---

> KuCoin is the most popular bitcoin exchange that you can buy and sell bitcoin
  securely.

This app is the interface to an exchange. Exchanges are all custodial which
makes the app **not verifiable**.
