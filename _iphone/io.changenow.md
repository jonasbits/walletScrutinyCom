---
wsId: ChangeNOW
title: "ChangeNOW Crypto Exchange"
altTitle: 
authors:
- leo
appId: io.changenow
idd: 1518003605
released: 2020-06-26
updated: 2021-02-15
version: "1.4.1"
score: 4.37984
reviews: 129
size: 29969408
developerWebsite: 
repository: 
issue: 
icon: io.changenow.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

