---
wsId: bitpaytrading
title: "BitBay - Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: com.bitbay.BitBayTrading
idd: 1409644952
released: 2018-11-17
updated: 2020-11-04
version: "1.3.18"
score: 3.1875
reviews: 16
size: 96790528
developerWebsite: https://bitbay.net
repository: 
issue: 
icon: com.bitbay.BitBayTrading.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

