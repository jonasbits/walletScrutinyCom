---
wsId: Remitano
title: "Remitano"
altTitle: 
authors:
- leo
appId: com.remitano.remitano
idd: 1116327021
released: 2016-05-25
updated: 2021-03-04
version: "5.16.0"
score: 4.75885
reviews: 6183
size: 59552768
developerWebsite: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: remitano
providerLinkedIn: company/Remitano
providerFacebook: remitano
providerReddit: 

redirect_from:

---

This app is an interface to an exchange which holds your coins. On the App Store
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.
