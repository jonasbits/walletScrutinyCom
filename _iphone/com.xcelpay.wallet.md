---
wsId: XcelPay
title: "XcelPay - Secure Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.xcelpay.wallet
idd: 1461215417
released: 2019-05-23
updated: 2021-02-26
version: "2.18.7"
score: 4
reviews: 18
size: 39463936
developerWebsite: http://xcelpay.io
repository: 
issue: 
icon: com.xcelpay.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

