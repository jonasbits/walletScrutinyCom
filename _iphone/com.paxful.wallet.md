---
wsId: Paxful
title: "Paxful Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.paxful.wallet
idd: 1443813253
released: 2019-05-06
updated: 2021-03-08
version: "1.8.4"
score: 3.96628
reviews: 2224
size: 67895296
developerWebsite: https://paxful.com/
repository: 
issue: 
icon: com.paxful.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

