---
wsId: YouHodler
title: "YouHodler - Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.youhodler.youhodler
idd: 1469351696
released: 2019-07-22
updated: 2021-02-15
version: "2.10.0"
score: 4.70492
reviews: 122
size: 35258368
developerWebsite: https://www.youhodler.com/
repository: 
issue: 
icon: com.youhodler.youhodler.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

