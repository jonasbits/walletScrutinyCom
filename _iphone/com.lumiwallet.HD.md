---
wsId: LumiWallet
title: "Bitcoin Wallet by Lumi Wallet"
altTitle: 
authors:
- leo
appId: com.lumiwallet.HD
idd: 1316477906
released: 2017-12-05
updated: 2021-03-03
version: "3.9.8"
score: 4.85951
reviews: 3253
size: 74719232
developerWebsite: https://lumiwallet.com/
repository: 
issue: 
icon: com.lumiwallet.HD.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

