---
wsId: CashApp
title: "Cash App"
altTitle: 
authors:
- leo
appId: com.squareup.cash
idd: 711923939
released: 2013-10-13
updated: 2021-03-04
version: "3.34.1"
score: 4.74301
reviews: 1615284
size: 222208000
developerWebsite: https://cash.app
repository: 
issue: 
icon: com.squareup.cash.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

On their website the provider claims:

> **Coin Storage**<br>
  Your Bitcoin balance is securely stored in our offline system

which means it is custodial.
