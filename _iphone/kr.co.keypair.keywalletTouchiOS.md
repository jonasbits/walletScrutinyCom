---
wsId: krcokeypair
title: "KeyWallet Touch - Bitcoin"
altTitle: 
authors:

appId: kr.co.keypair.keywalletTouchiOS
idd: 1473941321
released: 2019-07-25
updated: 2020-12-14
version: "1.1.27"
score: 4
reviews: 1
size: 20524032
developerWebsite: http://keywalletpro.io
repository: 
issue: 
icon: kr.co.keypair.keywalletTouchiOS.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

<!-- nosource -->
As far as we can see, this is the same as
[this app](/android/kr.co.keypair.keywalletTouch) and thus is **not verifiable**.
