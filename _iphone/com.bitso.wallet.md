---
wsId: bitso
title: "Bitso - Buy and sell bitcoin"
altTitle: 
authors:
- leo
appId: com.bitso.wallet
idd: 1292836438
released: 2018-02-16
updated: 2020-12-24
version: "2.16.0"
score: 3.7027
reviews: 37
size: 93405184
developerWebsite: https://bitso.com/app
repository: 
issue: 
icon: com.bitso.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

