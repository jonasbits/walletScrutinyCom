---
wsId: 
title: "Crypto.com l DeFi Wallet"
altTitle: 
authors:
- leo
appId: com.defi.wallet
idd: 1512048310
released: 2020-05-17
updated: 2021-02-26
version: "1.7.1"
score: 4.25767
reviews: 489
size: 64153600
developerWebsite: https://crypto.com/en/defi/wallet/
repository: 
issue: 
icon: com.defi.wallet.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: company/cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

This app's description is promising:

> Decentralized:
> - Gain full control of your crypto and private keys [...]

On their website though we cannot find any links to source code.

Searching their `appId` on GitHub,
[yields nothing](https://github.com/search?q=%22com.defi.wallet%22) neither.

This brings us to the verdict: **not verifiable**.
