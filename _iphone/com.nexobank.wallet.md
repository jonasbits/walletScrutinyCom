---
wsId: nexo
title: "Nexo - Crypto Banking Account"
altTitle: 
authors:
- leo
appId: com.nexobank.wallet
idd: 1455341917
released: 2019-06-27
updated: 2021-03-05
version: "1.4.0"
score: 3.95896
reviews: 268
size: 34356224
developerWebsite: https://nexo.io
repository: 
issue: 
icon: com.nexobank.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

