---
wsId: ShapeShift
title: "ShapeShift: Buy & Trade Crypto"
altTitle: 
authors:
- leo
appId: com.shapeShift.shapeShift
idd: 996569075
released: 2015-06-06
updated: 2021-03-02
version: "2.12.0"
score: 2.97403
reviews: 385
size: 77875200
developerWebsite: https://shapeshift.com
repository: 
issue: 
icon: com.shapeShift.shapeShift.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

