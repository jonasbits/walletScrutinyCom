---
wsId: 
title: "ezDeFi-Crypto & Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.ezdefi.nexty
idd: 1492046549
released: 2019-12-15
updated: 2021-02-11
version: "0.3.2"
score: 5
reviews: 6
size: 59666432
developerWebsite: https://ezdefi.com/
repository: 
issue: 
icon: com.ezdefi.nexty.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

