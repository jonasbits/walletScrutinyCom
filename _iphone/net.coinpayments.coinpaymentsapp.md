---
wsId: CoinPayments
title: "CoinPayments - Crypto Wallet"
altTitle: 
authors:
- leo
appId: net.coinpayments.coinpaymentsapp
idd: 1162855939
released: 2019-02-04
updated: 2021-01-21
version: "2.2.1"
score: 4.09375
reviews: 32
size: 133352448
developerWebsite: https://www.coinpayments.net/
repository: 
issue: 
icon: net.coinpayments.coinpaymentsapp.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

