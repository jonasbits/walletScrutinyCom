---
wsId: Bitstamp
title: "Bitstamp – crypto exchange app"
altTitle: 
authors:
- leo
appId: net.bitstamp
idd: 1406825640
released: 2019-01-27
updated: 2021-01-13
version: "1.6.4"
score: 4.81649
reviews: 4278
size: 82498560
developerWebsite: https://www.bitstamp.net/
repository: 
issue: 
icon: net.bitstamp.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Bitstamp
providerLinkedIn: company/bitstamp
providerFacebook: Bitstamp
providerReddit: 

redirect_from:

---

[Just like on Google Play](/android/net.bitstamp.app), they claim:

> Convenient, but secure<br>
  ● We store 98% of all crypto assets in cold storage

which means you don't get the keys for your coins. This is a custodial service
and therefore **not verifiable**.
