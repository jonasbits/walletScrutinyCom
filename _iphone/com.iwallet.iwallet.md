---
wsId: iwallet
title: "iWallet"
altTitle: 
authors:
- leo
appId: com.iwallet.iwallet
idd: 1479545928
released: 2019-09-11
updated: 2021-02-23
version: "2.34"
score: 4.44444
reviews: 36
size: 52318208
developerWebsite: 
repository: 
issue: 
icon: com.iwallet.iwallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

