---
wsId: ZenGo
title: "ZenGo: Crypto & Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: kzencorp.mobile.ios
idd: 1440147115
released: 2019-06-04
updated: 2021-02-28
version: "2.21.2"
score: 4.60018
reviews: 1108
size: 71150592
developerWebsite: https://www.zengo.com
repository: 
issue: 
icon: kzencorp.mobile.ios.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

