---
wsId: CoinbaseWallet
title: "Coinbase Wallet"
altTitle: 
authors:
- leo
appId: org.toshi.distribution
idd: 1278383455
released: 2017-09-24
updated: 2021-03-01
version: "23.8"
score: 4.66696
reviews: 20574
size: 136266752
developerWebsite: https://wallet.coinbase.com
repository: 
issue: 
icon: org.toshi.distribution.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinbaseWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This is the iPhone version of the
[Android Coinbase Wallet — Crypto Wallet & DApp Browser](/android/org.toshi).

Just like the Android version, this wallet is **not verifiable**.
