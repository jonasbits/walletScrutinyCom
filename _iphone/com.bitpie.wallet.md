---
wsId: bitpie
title: "Bitpie-Universal Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.bitpie.wallet
idd: 1481314229
released: 2019-09-28
updated: 2021-03-04
version: "5.0.016"
score: 3.21053
reviews: 19
size: 275860480
developerWebsite: 
repository: 
issue: 
icon: com.bitpie.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

