---
wsId: Vidulum
title: "Vidulum"
altTitle: 
authors:
- leo
appId: com.vidulum.app
idd: 1505859171
released: 2020-07-25
updated: 2021-03-06
version: "1.1.6"
score: 5
reviews: 4
size: 60460032
developerWebsite: https://vidulum.app
repository: 
issue: 
icon: com.vidulum.app.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

