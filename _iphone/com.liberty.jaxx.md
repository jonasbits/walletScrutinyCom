---
wsId: jaxxliberty
title: "Jaxx Liberty Blockchain Wallet"
altTitle: 
authors:
- leo
appId: com.liberty.jaxx
idd: 1435383184
released: 2018-09-30
updated: 2021-03-02
version: "2.6.1"
score: 4.50424
reviews: 1178
size: 46631936
developerWebsite: https://jaxx.io
repository: 
issue: 
icon: com.liberty.jaxx.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

