---
wsId: ownbit
title: "Ownbit - BTC & ETH Cold Wallet"
altTitle: 
authors:
- leo
appId: com.bitbill.wallet
idd: 1321798216
released: 2018-02-04
updated: 2021-03-05
version: "4.27.0"
score: 4.53061
reviews: 49
size: 112986112
developerWebsite: 
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

