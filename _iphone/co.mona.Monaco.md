---
wsId: mona
title: "Crypto.com - Buy Bitcoin Now"
altTitle: 
authors:
- leo
appId: co.mona.Monaco
idd: 1262148500
released: 2017-08-28
updated: 2021-03-06
version: "3.85.1"
score: 4.42855
reviews: 12953
size: 271464448
developerWebsite: https://crypto.com/
repository: 
issue: 
icon: co.mona.Monaco.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: company/cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

As their [version for Android](/android/co.mona.android) this app is custodial
and thus **not verifiable**.
