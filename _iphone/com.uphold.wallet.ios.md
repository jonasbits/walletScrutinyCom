---
wsId: UpholdbuyandsellBitcoin
title: "Uphold: buy and sell Bitcoin"
altTitle: 
authors:
- leo
appId: com.uphold.wallet.ios
idd: 1101145849
released: 2016-04-16
updated: 2021-02-26
version: "4.15.11"
score: 4.01159
reviews: 3795
size: 63840256
developerWebsite: 
repository: 
issue: 
icon: com.uphold.wallet.ios.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

