---
wsId: 
title: "SafePal Wallet"
altTitle: 
authors:
- leo
appId: walletapp.safepal.io
idd: 1548297139
released: 2021-01-08
updated: 2021-02-24
version: "2.5.7"
score: 3.95833
reviews: 48
size: 113358848
developerWebsite: https://www.safepal.io/
repository: 
issue: 
icon: walletapp.safepal.io.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-13
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

