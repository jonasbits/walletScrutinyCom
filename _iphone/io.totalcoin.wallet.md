---
wsId: Totalcoin
title: "Bitcoin Wallet App - Totalcoin"
altTitle: 
authors:
- leo
appId: io.totalcoin.wallet
idd: 1392398906
released: 2018-07-02
updated: 2021-01-27
version: "2.9.6"
score: 4.55224
reviews: 67
size: 55388160
developerWebsite: 
repository: 
issue: 
icon: io.totalcoin.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

