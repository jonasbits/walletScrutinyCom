---
wsId: 
title: "Multi Crypto Wallet－Freewallet"
altTitle: 
authors:
- leo
appId: mw.org.freewallet.app
idd: 1274003898
released: 2017-08-29
updated: 2021-01-19
version: "1.15.0"
score: 4.11534
reviews: 867
size: 45145088
developerWebsite: https://freewallet.org
repository: 
issue: 
icon: mw.org.freewallet.app.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

