---
wsId: InfinitoWallet
title: "Infinito Wallet - Crypto Safe"
altTitle: 
authors:
- leo
appId: io.infinito.wallet
idd: 1315572736
released: 2018-01-14
updated: 2020-12-16
version: "2.35.0"
score: 4.2948
reviews: 173
size: 105237504
developerWebsite: 
repository: 
issue: 
icon: io.infinito.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

