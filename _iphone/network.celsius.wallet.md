---
wsId: Celsius
title: "Celsius - Crypto Wallet"
altTitle: 
authors:
- leo
appId: network.celsius.wallet
idd: 1387885523
released: 2018-06-17
updated: 2021-02-25
version: "4.8.0"
score: 4.19674
reviews: 798
size: 45212672
developerWebsite: https://celsius.network/app
repository: 
issue: 
icon: network.celsius.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

