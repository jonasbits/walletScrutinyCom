---
wsId: BitWallet
title: "BitWallet - Buy & Sell Bitcoin"
altTitle: 
authors:
- leo
appId: Tissatech.Bit-Wallet
idd: 1331439005
released: 2019-02-06
updated: 2021-02-27
version: "1.5.9"
score: 4.85907
reviews: 738
size: 15589376
developerWebsite: 
repository: 
issue: 
icon: Tissatech.Bit-Wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

