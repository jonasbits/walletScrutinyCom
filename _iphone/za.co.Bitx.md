---
wsId: Luno
title: "Luno Bitcoin & Cryptocurrency"
altTitle: 
authors:
- leo
appId: za.co.Bitx
idd: 927362479
released: 2014-10-31
updated: 2021-02-25
version: "7.8.0"
score: 4.43337
reviews: 3129
size: 89120768
developerWebsite: https://www.luno.com
repository: 
issue: 
icon: za.co.Bitx.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

