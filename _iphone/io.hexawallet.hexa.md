---
wsId: Hexa
title: "Bitcoin Wallet Hexa"
altTitle: 
authors:
- leo
appId: io.hexawallet.hexa
idd: 1490205837
released: 2020-03-13
updated: 2021-02-23
version: "1.4.5"
score: 4.75
reviews: 4
size: 47112192
developerWebsite: https://hexawallet.io/
repository: 
issue: 
icon: io.hexawallet.hexa.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

