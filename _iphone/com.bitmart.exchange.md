---
wsId: bitmart
title: "BitMart - Crypto Exchange"
altTitle: 
authors:
- leo
appId: com.bitmart.exchange
idd: 1396382871
released: 2018-07-30
updated: 2021-02-05
version: "2.4.5"
score: 3.25424
reviews: 59
size: 176228352
developerWebsite: https://www.bitmart.com/
repository: 
issue: 
icon: com.bitmart.exchange.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

