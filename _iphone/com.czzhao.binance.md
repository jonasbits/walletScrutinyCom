---
wsId: binance
title: "Binance: Buy Bitcoin Securely"
altTitle: 
authors:
- leo
appId: com.czzhao.binance
idd: 1436799971
released: 2018-10-03
updated: 2021-03-04
version: "2.26.2"
score: 4.53762
reviews: 36182
size: 466760704
developerWebsite: https://www.binance.com
repository: 
issue: 
icon: com.czzhao.binance.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: binance
providerLinkedIn: 
providerFacebook: binance
providerReddit: binance

redirect_from:

---

In the description the provider claims:

> Your funds are protected by our Secure Asset Fund for Users (SAFU Funds) which
  means we have your back.

which sounds very custodial and as such the app is **not verifiable**.
