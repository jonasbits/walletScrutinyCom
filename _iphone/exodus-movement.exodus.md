---
wsId: ExodusCryptoBitcoinWallet
title: "Exodus: Crypto Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: exodus-movement.exodus
idd: 1414384820
released: 2019-03-20
updated: 2021-03-02
version: "21.3.1"
score: 4.6284
reviews: 8146
size: 28694528
developerWebsite: https://exodus.com/mobile
repository: 
issue: 
icon: exodus-movement.exodus.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: exodus_io
providerLinkedIn: 
providerFacebook: exodus.io
providerReddit: 

redirect_from:

---

Just like [their Android wallet](/android/exodusmovement.exodus/), this app is
closed source and thus **not verifiable**.
