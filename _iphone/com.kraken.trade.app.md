---
wsId: krakent
title: "Kraken Pro"
altTitle: 
authors:
- leo
appId: com.kraken.trade.app
idd: 1473024338
released: 2019-11-09
updated: 2021-02-01
version: "1.5.11"
score: 4.59429
reviews: 4846
size: 33881088
developerWebsite: 
repository: 
issue: 
icon: com.kraken.trade.app.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

