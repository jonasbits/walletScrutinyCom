---
wsId: SpotWalletapp
title: "Buy Bitcoin - Spot Wallet app"
altTitle: 
authors:
- leo
appId: tech.spotapp.spot
idd: 1390560448
released: 2018-08-04
updated: 2021-03-08
version: "3.1.1"
score: 4.61655
reviews: 2827
size: 81015808
developerWebsite: https://spot-bitcoin.com
repository: 
issue: 
icon: tech.spotapp.spot.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

