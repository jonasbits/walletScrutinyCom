---
wsId: Sylo
title: "Sylo"
altTitle: 
authors:
- leo
appId: io.sylo.dapp
idd: 1452964749
released: 2019-09-07
updated: 2021-02-27
version: "3.0.10"
score: 4.61538
reviews: 13
size: 182617088
developerWebsite: https://www.sylo.io/wallet/
repository: 
issue: 
icon: io.sylo.dapp.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

