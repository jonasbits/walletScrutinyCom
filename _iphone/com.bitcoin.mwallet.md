---
wsId: mwallet
title: "Bitcoin Wallet: buy BTC & BCH"
altTitle: 
authors:
- leo
appId: com.bitcoin.mwallet
idd: 1252903728
released: 2017-07-08
updated: 2021-02-24
version: "6.11.4"
score: 4.25006
reviews: 4415
size: 122456064
developerWebsite: https://www.bitcoin.com
repository: 
issue: 
icon: com.bitcoin.mwallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

