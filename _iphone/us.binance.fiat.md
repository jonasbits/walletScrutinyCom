---
wsId: BinanceUS
title: "Binance.US - Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: us.binance.fiat
idd: 1492670702
released: 2020-01-02
updated: 2021-02-11
version: "2.3.2"
score: 4.22814
reviews: 23898
size: 118355968
developerWebsite: https://www.binance.us
repository: 
issue: 
icon: us.binance.fiat.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: binanceus
providerLinkedIn: company/binance-us
providerFacebook: BinanceUS
providerReddit: 

redirect_from:

---

This is the iPhone version of [this Android app](/android/com.binance.us) and we
come to the same conclusion for the same reasons. This app is **not verifiable**.
