---
wsId: crypterium
title: "Crypterium | Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.Crypterium.Crypterium
idd: 1360632912
released: 2018-03-23
updated: 2021-03-02
version: "1.14.8"
score: 4.52489
reviews: 964
size: 248443904
developerWebsite: https://cards.crypterium.com/visa
repository: 
issue: 
icon: com.Crypterium.Crypterium.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

