---
title: "MEW wallet – Ethereum wallet"
altTitle: 

users: 100000
appId: com.myetherwallet.mewwallet
launchDate: 
latestUpdate: 2021-02-04
apkVersionName: "1.2.4"
stars: 3.9
ratings: 3283
reviews: 1378
size: 43M
website: http://mewwallet.com
repository: 
issue: 
icon: com.myetherwallet.mewwallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


