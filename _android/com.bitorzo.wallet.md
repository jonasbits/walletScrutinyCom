---
wsId: 
title: "Bitcoin Wallet"
altTitle: 
authors:

users: 1000
appId: com.bitorzo.wallet
launchDate: 
latestUpdate: 2021-01-03
apkVersionName: "3.1.3"
stars: 4.1
ratings: 22
reviews: 19
size: 12M
website: 
repository: 
issue: 
icon: com.bitorzo.wallet.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-03
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/183 -->
