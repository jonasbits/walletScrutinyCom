---
wsId: krcokeypair
title: "NiXPAY"
altTitle: 
authors:

users: 100
appId: kr.co.keypair.nixtouch
launchDate: 
latestUpdate: 2019-12-27
apkVersionName: "1.0.0.65"
stars: 0.0
ratings: 
reviews: 
size: 11M
website: https://www.nixblock.com
repository: 
issue: 
icon: kr.co.keypair.nixtouch.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- nosource -->
As far as we can see, this is the same as
[this app](/android/kr.co.keypair.keywalletTouch) and thus is **not verifiable**.
