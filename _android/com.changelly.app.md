---
title: "Changelly: Instant Bitcoin and Crypto Exchange"
altTitle: 

users: 50000
appId: com.changelly.app
launchDate: 
latestUpdate: 2020-06-15
apkVersionName: "2.6"
stars: 4.5
ratings: 759
reviews: 451
size: 13M
website: 
repository: 
issue: 
icon: com.changelly.app.png
bugbounty: 
verdict: nowallet # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.changelly.app/
  - /posts/com.changelly.app/
---


This app has no wallet feature in the sense that you hold Bitcoins in the app.
