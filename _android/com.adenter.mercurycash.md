---
wsId: mercurycash
title: "Mercury Cash"
altTitle: 
authors:
- leo
users: 10000
appId: com.adenter.mercurycash
launchDate: 
latestUpdate: 2021-03-08
apkVersionName: "5.0.1"
stars: 4.1
ratings: 179
reviews: 127
size: 82M
website: http://mercury.cash
repository: 
issue: 
icon: com.adenter.mercurycash.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-08-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: mercurycash
providerLinkedIn: 
providerFacebook: mercurycash
providerReddit: 

redirect_from:
  - /com.adenter.mercurycash/
---


This app makes no claims about self-custody so we have to assume it is a
custodial product and thus **not verifiable**.
