---
wsId: 
title: "Altana - Bitcoin Wallet"
altTitle: 
authors:

users: 1000
appId: it.inbitcoin.altana
launchDate: 
latestUpdate: 2021-02-27
apkVersionName: "Varies with device"
stars: 4.4
ratings: 47
reviews: 21
size: Varies with device
website: 
repository: 
issue: 
icon: it.inbitcoin.altana.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/165 -->
