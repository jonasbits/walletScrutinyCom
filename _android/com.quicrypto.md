---
title: "Quicrypto: Earn Crypto & Free Bitcoin"
altTitle: 

users: 100000
appId: com.quicrypto
launchDate: 
latestUpdate: 2020-11-23
apkVersionName: "2.77.1"
stars: 4.3
ratings: 3158
reviews: 2005
size: 26M
website: http://www.quicrypto.com
repository: 
issue: 
icon: com.quicrypto.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: false
signer: 
reviewArchive:


providerTwitter: quicrypto
providerLinkedIn: 
providerFacebook: Quicrypto
providerReddit: 

redirect_from:
  - /com.quicrypto/
---


This app "pays" you in Bitcoin and Nano for tasks

> What is Quicrypto?
> 
> Quicrypto is a “reward app” that allows its users to earn money (in the form of the Nano cryptocurrency) by watching ads and completeing offers.

and one of the early tasks appears to be to give a five star rating:

> Filip Zdravkovic
> 
> What a scam.Unfortunatelly people still fall for it and the only reason it has
  a good rating is because they promise you a some coins for it.Do not download
  this useless virus app.

They also have instructions on how to get a wallet, so ... it's not a wallet
itself.
