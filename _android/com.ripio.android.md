---
wsId: 
title: "Ripio Bitcoin Wallet: the new digital economy"
altTitle: 
authors:
- leo
users: 1000000
appId: com.ripio.android
launchDate: 2015-05-29
latestUpdate: 2021-03-03
apkVersionName: "4.3.8"
stars: 3.9
ratings: 13688
reviews: 7535
size: 47M
website: https://www.ripio.com/ar/wallet
repository: 
issue: 
icon: com.ripio.android.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-03-25
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ripioapp
providerLinkedIn: company/ripio
providerFacebook: RipioApp
providerReddit: 

redirect_from:
  - /ripio/
  - /com.ripio.android/
  - /posts/2019/11/ripio/
  - /posts/com.ripio.android/
---


Ripio Bitcoin Wallet: the new digital economy
does not claim to be non-custodial and looks much like an interface for an
exchange. Neither can we find any source code.

Therefore: This wallet is **not verifiable**.
