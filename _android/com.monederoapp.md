---
wsId: 
title: "Monedero: Wallet Bitcoin, Ethereum, Dash"
altTitle: 
authors:
- leo
users: 1000
appId: com.monederoapp
launchDate: 
latestUpdate: 2021-02-22
apkVersionName: "2.2.0"
stars: 4.1
ratings: 10
reviews: 5
size: 10M
website: 
repository: 
issue: 
icon: com.monederoapp.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.monederoapp/
  - /posts/com.monederoapp/
---


This page was created by a script from the **appId** "com.monederoapp" and public
information found
[here](https://play.google.com/store/apps/details?id=com.monederoapp).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.
