---
wsId: getchange
title: "Change: Beginner-Friendly Trading & Investment App"
altTitle: 
authors:
- leo
users: 100000
appId: com.getchange.wallet.cordova
launchDate: 
latestUpdate: 2021-03-05
apkVersionName: "10.9.149"
stars: 4.1
ratings: 1658
reviews: 798
size: 32M
website: https://getchange.com
repository: 
issue: 
icon: com.getchange.wallet.cordova.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-26
reviewStale: true
signer: 
reviewArchive:


providerTwitter: changefinance
providerLinkedIn: company/changeinvest
providerFacebook: changeinvest
providerReddit: 

redirect_from:
  - /com.getchange.wallet.cordova/
  - /posts/com.getchange.wallet.cordova/
---


On their Google Play description we find

> • Secure: Funds are protected in multi-signature, cold-storage cryptocurrency
  wallets

which means it is a custodial service and thus **not verifiable**.
