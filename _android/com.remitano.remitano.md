---
wsId: Remitano
title: "Remitano - Buy & Sell Bitcoin Fast & Securely"
altTitle: 
authors:
- leo
users: 100000
appId: com.remitano.remitano
launchDate: 
latestUpdate: 2021-03-04
apkVersionName: "5.16.0"
stars: 4.1
ratings: 10968
reviews: 5189
size: 32M
website: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-26
reviewStale: true
signer: 
reviewArchive:


providerTwitter: remitano
providerLinkedIn: company/Remitano
providerFacebook: remitano
providerReddit: 

redirect_from:
  - /posts/com.remitano.remitano/
---


This app is an interface to an exchange which holds your coins. On Google Play
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.
