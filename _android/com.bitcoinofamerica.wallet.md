---
wsId: 
title: "Bitcoin of America Wallet"
altTitle: 
authors:

users: 10000
appId: com.bitcoinofamerica.wallet
launchDate: 
latestUpdate: 2020-04-01
apkVersionName: "1.1.2"
stars: 4.3
ratings: 137
reviews: 44
size: 55M
website: 
repository: 
issue: 
icon: com.bitcoinofamerica.wallet.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/186 -->
