---
wsId: 
title: "Lykke: Trade, Buy & Store Bitcoin, Crypto and More"
altTitle: 
authors:

users: 50000
appId: com.lykkex.LykkeWallet
launchDate: 
latestUpdate: 2020-04-07
apkVersionName: "12.7.7"
stars: 3.5
ratings: 564
reviews: 344
size: 23M
website: 
repository: 
issue: 
icon: com.lykkex.LykkeWallet.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/185 -->
