---
wsId: 
title: "Bitcoin wallet"
altTitle: 
authors:

users: 5000
appId: info.mysecurewallet.wallet
launchDate: 
latestUpdate: 2019-11-02
apkVersionName: "1.1"
stars: 3.6
ratings: 48
reviews: 37
size: 3.9M
website: 
repository: 
issue: 
icon: info.mysecurewallet.wallet.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/188 -->
