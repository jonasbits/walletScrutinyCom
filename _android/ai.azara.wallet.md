---
wsId: 
title: "Azara Crypto & Bitcoin Wallet: Buy, Sell, Exchange"
altTitle: 
authors:
- leo
users: 1000
appId: ai.azara.wallet
launchDate: 
latestUpdate: 2021-02-21
apkVersionName: "0.01.08"
stars: 4.5
ratings: 37
reviews: 35
size: 45M
website: 
repository: 
issue: 
icon: ai.azara.wallet.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /ai.azara.wallet/
---


