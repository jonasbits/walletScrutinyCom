---
wsId: huobi
title: "Huobi Global"
altTitle: 
authors:
- leo
users: 1000000
appId: pro.huobi
launchDate: 
latestUpdate: 2021-02-25
apkVersionName: "6.1.6"
stars: 4.4
ratings: 8009
reviews: 3029
size: 63M
website: https://www.huobi.com/en-us
repository: 
issue: 
icon: pro.huobi.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-26
reviewStale: true
signer: 
reviewArchive:


providerTwitter: HuobiGlobal
providerLinkedIn: 
providerFacebook: huobiglobalofficial
providerReddit: 

redirect_from:
  - /pro.huobi/
  - /posts/pro.huobi/
---


Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
