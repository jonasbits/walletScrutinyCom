---
wsId: 
title: "ProBit Global: Buy & Sell Bitcoin. Crypto Exchange"
altTitle: 
authors:
- leo
users: 100000
appId: com.probit.app.android2.release.global
launchDate: 
latestUpdate: 2021-02-09
apkVersionName: "1.31"
stars: 3.5
ratings: 3001
reviews: 1977
size: 17M
website: https://www.probit.com
repository: 
issue: 
icon: com.probit.app.android2.release.global.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-26
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ProBit_Exchange
providerLinkedIn: company/probit-exchange
providerFacebook: probitexchange
providerReddit: 

redirect_from:
  - /com.probit.app.android2.release.global/
---


Probit appears to also and mainly be an exchange and as we can't find claims to
the contrary, we assume this app is a custodial offering and thus **not verifiable**.
