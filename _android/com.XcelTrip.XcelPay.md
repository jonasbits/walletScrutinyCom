---
wsId: XcelPay
title: "XcelPay - Secure Bitcoin & Ethereum Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.XcelTrip.XcelPay
launchDate: 
latestUpdate: 2021-02-26
apkVersionName: "2.18.7"
stars: 4.3
ratings: 400
reviews: 255
size: 31M
website: http://www.xcelpay.io
repository: 
issue: 
icon: com.XcelTrip.XcelPay.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: XcelPayWallet
providerLinkedIn: in/xcel-pay-1b6228172
providerFacebook: xcelpay
providerReddit: 

redirect_from:
  - /com.XcelTrip.XcelPay/
---


This wallet has no claim of being non-custodial on Google Play.

The one-star ratings over and over tell:

* there is a referral program, promising rewards
* the rewards are never reflected in the wallet
* funds cannot be sent to a different wallet
* SCAM

