---
wsId: krcokeypair
title: "Purple Touch"
altTitle: 
authors:

users: 100
appId: kr.co.keypair.purplecardtouch
launchDate: 
latestUpdate: 2019-07-17
apkVersionName: "1.0.0.45"
stars: 0.0
ratings: 
reviews: 
size: 9.6M
website: http://www.banco.id
repository: 
issue: 
icon: kr.co.keypair.purplecardtouch.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- nosource -->
As far as we can see, this is the same as
[this app](/android/kr.co.keypair.keywalletTouch) and thus is **not verifiable**.
