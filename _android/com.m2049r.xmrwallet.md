---
wsId: 
title: "monerujo: Monero Wallet"
altTitle: 
authors:
- leo
users: 50000
appId: com.m2049r.xmrwallet
launchDate: 
latestUpdate: 2021-02-20
apkVersionName: "1.17.7 'Druk'"
stars: 3.1
ratings: 638
reviews: 387
size: Varies with device
website: https://monerujo.io
repository: 
issue: 
icon: com.m2049r.xmrwallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-27
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app does not feature BTC wallet functionality.